<?php
session_start();
if(isset($_SESSION["email"])) {
    $email = $_SESSION["email"];
}

if(isset($_SESSION["userId"])) {
    $userId = $_SESSION["userId"];
}

if (empty($email) || empty($userId)) {
    header("Location: login.php");
    exit;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Facebook app</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>

<body>
<div class="home-wrapper">
    <?php
        include_once("header.php");
    ?>
    <div class="home-wrapper__main">
        <div class="user-action">
            <ul class="user-action-list">
                <li class="user-action-item">
                    <div class="user-info">
                        <img class="user-info__image" src="">
                        <span class="user-info__name"></span>
                    </div>
                </li>
                <li class="user-action-item">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/y8/r/S0U5ECzYUSu.png">
                    <span>Friends</span>
                </li>
                <li class="user-action-item">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/yh/r/SeXJIAlf_z-.png">
                    <span>Messenger</span>
                </li>
                <li class="user-action-item">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/y5/r/PrjLkDYpYbH.png">
                    <span>Group</span>
                </li>
                <li class="user-action-item">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/lVijPkTeN-r.png">
                    <span>Saved</span>
                </li>
                <li class="user-action-item">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/yK/r/mAnT0r8GSOm.png">
                    <span>Favorites</span>
                </li>
                <li class="user-action-item">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/yC/r/bo0Zt72NIra.png">
                    <span>Weather</span>
                </li>
            </ul>
        </div>
        <div class="post">
            <div class="post__add">
                <span class="post-title">Create post</span>
                <input class="post-input input-form" id="post_message" type="text" placeholder="What's on your mind?">
                <img id="post_image" class="post-image" src="">
                <form id="post-action" class="post-action">
                    <a id="btn-add-post" class="btn-add-post">Post message</a>
                    <input type="file" name="file" id="upload-image" onchange="uploadImage()">
                    <label for="upload-image">
                            <span href="#" class="btn-upload-image">
                                Photo/Video
                            </span>
                    </label>
                </form>
            </div>
            <ul class="post__list">

            </ul>
        </div>
        <div class="user-friend">
            <div class="user-friend-box">
                <div class="title">
                    <img class="icon" src="https://static.xx.fbcdn.net/rsrc.php/v3/y8/r/S0U5ECzYUSu.png">
                    <span>Friends</span>
                </div>
                <ul class="user-friend-list">

                </ul>
            </div>
        </div>
        <div class="message-box-list">

        </div>
    </div>
</div>
</div>
</body>

<script>
    var userId = '<?php echo $userId ?>';

    retrievePost();
    retrieveListFriend();

    $("#btn-add-post").click(function () {
        addPost();
    })

    function addPost() {
        var postMessage = $("#post_message").val();
        var postImage = $("#post_image").attr("src");

        $.ajax({
            url: "controller/UserController.php",
            method: "POST",
            data: {
                post_message: postMessage,
                post_image_path: postImage,
                userId: userId,
                action: "addPost"
            },
            success: function (createdPost) {
                createdPost = JSON.parse(createdPost);

                if (createdPost.isSuccess) {
                    // retrievePost();
                }
            }
        });
    }

    function retrievePost() {
        $.ajax({
            url: "controller/UserController.php",
            method: "GET",
            data: {
                userId: userId,
                action: "retrievePost"
            },
            success: function (posts) {
                posts = JSON.parse(posts);
                $('.post__list li').remove();

                let attemp = (index = 0) => {
                    var post = posts[index];

                    if (!post) {
                        return;
                    }

                    post.post_image = post.post_image ? post.post_image : "";
                    if (isUrlValid(post.post_message)) {
                        post.post_message = `<a class="post-message-link" href="${post.post_message}" target="_blank">${post.post_message}</a>`
                    }

                    var liHtml = `<li class="post-item">
                    <div class="poster-info">
                        <img class="poster-info__image" src="${post.user_image}">
                        <div class="poster-info__description">
                            <span class="name">${post.user_firstname} ${post.user_lastname}</span>
                            <span class="time">${post.created}</span>
                        </div>
                    </div>
                    <p class="post-item-message">
                        ${post.post_message}
                    </p>
                    <ul class="post-item-image-list-${post.id}"></ul>
                    <div class="post-action">
                        <a href="#" class="like">Like</a>
                        <a href="#" class="comment">Comment</a>
                        <a href="#" class="share">Share</a>
                    </div>
                </li>`;

                    $(".post__list").append(liHtml);

                    $.ajax({
                        url: "controller/UserController.php",
                        method: "GET",
                        data: {
                            action: "retrieveImage",
                            postId: post.id
                        },
                        success: function (images) {
                            images = JSON.parse(images);

                            for(var image of images) {
                                $(`.post-item-image-list-${post.id} li`).remove();

                                $(`.post-item-image-list-${post.id}`).append(`<img class="post-item-image" src="${image.image_path}">`)
                            }

                            index = index + 1;
                            attemp(index);
                        }
                    })
                }

                attemp();
            }
        });
    }

    function uploadImage() {
        var form = document.getElementById("post-action");

        $.ajax({
            url: "FileController.php",
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function (result)
            {
                result = JSON.parse(result);

                $("#post_image").attr("src", result.path);
            }
        });
    }

    function retrieveListFriend() {
        var userId = '<?php echo $userId ?>';

        $.ajax({
            url: "controller/UserController.php",
            method: "GET",
            data: {
                action: "retrieveListFriend",
                userId: userId
            },
            success: function (friends) {
                friends = JSON.parse(friends);

                $(".user-friend-list li").remove();

                for(var friend of friends) {
                    var liHtml = `<li id="friend-${friend.id}" class="user-friend-item" onclick="createMessageBox(${friend.id}, '${friend.user_firstname}', '${friend.user_lastname}', '${friend.user_image}')">
                        <div class="user-info">
                            <img class="user-info__image" src="${friend.user_image}">
                            <span class="user-info__name">${friend.user_firstname} ${friend.user_lastname}</span>
                        </div>
                    </li>`;

                    $(".user-friend-list").append(liHtml);
                }
            }
        })
    }

    function createMessageBox(id, firstname, lastname, image) {
        setInterval(() => {
            retrieveConversation(id);
        }, 1000);

        $(".message-box-list").append(`<div id="message-box-${id}" class="message-box">
                <div class="message-box__header">
                    <div class="user-info">
                        <img class="user-info__image" src="${image}">
                        <span class="user-info__name">${firstname} ${lastname}</span>
                    </div>
                    <svg class="icon-close" height="26px" width="26px" viewBox="-4 -4 24 24" onclick="removeMessageBox('message-box-${id}')"><line stroke="#bec2c9" stroke-linecap="round" stroke-width="2" x1="2" x2="14" y1="2" y2="14"></line><line stroke="#bec2c9" stroke-linecap="round" stroke-width="2" x1="2" x2="14" y1="14" y2="2"></line></svg>
                </div>
                <ol id="message-box__content-${id}" class="message-box__content">
                    <li class="line ours">Hi, babe!</li>
                    <li class="line ours">I have something for you.</li>
                    <li class="line">What is it?</li>
                    <li class="line ours">Just a little something.</li>
                </ol>
                <div class="message-box__footer">
                    <form id="message-box__input-${id}" class="message-box__input">
                    <input type="file" name="file" id="post-upload-image" onchange="sendAttachment(${id})">
                    <label for="post-upload-image">
                        <span class="btn-upload-image"></span>
                    </label>
                </form>
                <input onchange="sendMessage(this.value, ${id})" id="conversation-message-${id}" class="input-form" type="text" placeholder="Typing your message at here" autocomplete="off">
                <svg height="20px" width="20px" viewBox="0 0 24 24"><path d="M16.6915026,12.4744748 L3.50612381,13.2599618 C3.19218622,13.2599618 3.03521743,13.4170592 3.03521743,13.5741566 L1.15159189,20.0151496 C0.8376543,20.8006365 0.99,21.89 1.77946707,22.52 C2.41,22.99 3.50612381,23.1 4.13399899,22.8429026 L21.714504,14.0454487 C22.6563168,13.5741566 23.1272231,12.6315722 22.9702544,11.6889879 C22.8132856,11.0605983 22.3423792,10.4322088 21.714504,10.118014 L4.13399899,1.16346272 C3.34915502,0.9 2.40734225,1.00636533 1.77946707,1.4776575 C0.994623095,2.10604706 0.8376543,3.0486314 1.15159189,3.99121575 L3.03521743,10.4322088 C3.03521743,10.5893061 3.34915502,10.7464035 3.50612381,10.7464035 L16.6915026,11.5318905 C16.6915026,11.5318905 17.1624089,11.5318905 17.1624089,12.0031827 C17.1624089,12.4744748 16.6915026,12.4744748 16.6915026,12.4744748 Z" fill-rule="evenodd" stroke="none"></path></svg>
                </div>
            </div>`)
    }

    function removeMessageBox(messageBoxId) {
        $(`#${messageBoxId}`).remove();
    }

    function sendAttachment(receiver) {
        var form = document.getElementById(`message-box__input-${receiver}`);

        $.ajax({
            url: "FileController.php",
            type: "POST",
            data: new FormData(form),
            contentType: false,
            cache: false,
            processData: false,
            success: function (attachment)
            {
                attachment = JSON.parse(attachment);

                sendMessage("", receiver, attachment);
            }
        });

    }

    function sendMessage(value, receiver, attachment) {
        var userId = '<?php echo $userId ?>';

        if (attachment && attachment.path) {
            value = "";
        }
        if (value) {
            attachment = {};
            attachment.path = "";
        }

       if (value.length || (attachment && attachment.path.length)) {
           $.ajax({
               url: "controller/UserController.php",
               method: "POST",
               data: {
                   action: "sendMessage",
                   user_from: userId,
                   user_to: receiver,
                   message: value,
                   attachment: attachment.path
               },
               success: function (conversation) {
                   conversation = JSON.parse(conversation);

                   if (conversation.isSuccess) {
                       $(`#conversation-message-${receiver}`).val("");

                       if (conversation.attachment) {
                           if (['png', 'jpg', 'jpeg'].includes(attachment && attachment.extension)) {
                               $(`#message-box__content-${receiver}`).append(`<li class="line ours"><img width="160px" height="80px" src="${conversation.attachment}"></li>`)
                           } else {
                               $(`#message-box__content-${receiver}`).append(`<li class="line ours"><a href="${attachment.path}" target="_blank">${attachment.path}</a></li>`)
                           }
                       }
                       else {
                           $(`#message-box__content-${receiver}`).append(`<li class="line ours">${conversation.message}</li>`)
                       }
                   }
               }
           })
       }
    }

    function retrieveConversation(receiverId) {
        var userId = '<?php echo $userId ?>';

        $.ajax({
            url: "controller/UserController.php",
            method: "GET",
            data: {
                user_from: userId,
                user_to: receiverId,
                action: "retrieveConversation"
            },
            success: function (conversations) {
                conversations = JSON.parse(conversations);

                $(`#message-box__content-${receiverId} li`).remove();

                var liHtml = "";

                for(var conversation of conversations) {
                    if (conversation.attachment) {
                        var fileExtension = getFileExtension(conversation.attachment);

                        if (['png', 'jpg', 'jpeg'].includes(fileExtension)) {
                            if (conversation.user_from == userId) {
                                $(`#message-box__content-${receiverId}`).append(`<li class="line ours"><a href="${conversation.attachment}" target="_blank"><img width="160px" height="80px" src="${conversation.attachment}"></a></li>`)
                            } else {
                                $(`#message-box__content-${receiverId}`).append(`<li class="line"><a href="${conversation.attachment}" target="_blank"><img width="160px" height="80px" src="${conversation.attachment}"></a></li>`)
                            }
                        } else {
                            if (conversation.user_from == userId) {
                                $(`#message-box__content-${receiverId}`).append(`<li class="line ours"><a href="${conversation.attachment}" target="_blank">${conversation.attachment}</a></li>`)
                            } else {
                                $(`#message-box__content-${receiverId}`).append(`<li class="line"><a href="${conversation.attachment}" target="_blank">${conversation.attachment}</a></li>`)
                            }
                        }
                    }
                    else {
                        if (conversation.user_from == userId) {
                            $(`#message-box__content-${receiverId}`).append(`<li class="line ours">${conversation.message}</li>`)
                        }
                        else {
                            $(`#message-box__content-${receiverId}`).append(`<li class="line">${conversation.message}</li>`)
                        }
                    }
                }
            }
        });
    }

    function isUrlValid(url) {
        var res = url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);

        if(res) {
            return true;
        }
        else {
            return false;
        }
    }

    function getFileExtension(filename) {
        return filename.split('.').pop();
    }

</script>

</html>