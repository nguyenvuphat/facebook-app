<?php
session_start();

if (isset($_GET["textSearch"])) {
    $textSearch = $_GET["textSearch"];
}

if (isset($_SESSION["userId"])) {
    $userId = $_SESSION["userId"];
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Facebook app</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>

<body>
<div class="home-wrapper">
    <?php
        include_once("header.php");
    ?>
    <div class="search-friend">
        <span class="title">People</span>
        <ul class="search-list-friend">

        </ul>
    </div>
</div>
</body>
</html>

<script>
    retrieveUserByTextSearch();

    function retrieveUserByTextSearch() {
        var textSearch = '<?php echo $textSearch ?>';

        $.ajax({
            url: "controller/UserController.php",
            method: "GET",
            data: {
                action: "retrieveUserByTextSearch",
                textSearch: textSearch
            },
            success: function (users) {
                users = JSON.parse(users);

                $('.search-list-friend li').remove();

                for(var user of users) {

                    var liHtml = `<li class="friend-item">
                <div class="user-info">
                    <img class="user-info__image" src=${user.user_image}>
                    <span class="user-info__name">${user.user_firstname} ${user.user_lastname}</span>
                </div>
                <span id="request-friend-${user.id}-success" class="icon-box success"><span class="icon-friend-request-success"></span></span>
                <span id="request-friend-${user.id}" class="icon-box" onclick="onRequestFriend(${user.id})" ><span class="icon-friend-request"></span></span>
            </li>`

                    $(".search-list-friend").append(liHtml);
                }
            }
        })
    }

    function onRequestFriend(receiver) {
        var userId = '<?php echo $userId ?>';

        $.ajax({
            url: "controller/UserController.php",
            method: "POST",
            data: {
                action: "requestFriend",
                sender: userId,
                receiver: receiver
            },
            success: function (result) {
                result = JSON.parse(result);

                if (result.isSuccess) {
                    $("#request-friend-" + receiver).hide();
                    $("#request-friend-" + receiver + "-success").show();
                }
            }
        })
    }

</script>